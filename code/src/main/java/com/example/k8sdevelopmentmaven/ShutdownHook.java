package com.example.k8sdevelopmentmaven;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;

public class ShutdownHook implements ApplicationListener<ContextClosedEvent> {

    @Override
    public void onApplicationEvent(ContextClosedEvent event) {
        System.out.println("+++ Shutting down +++");
        throw new RuntimeException("Shutting down");
    }
}
