package com.example.k8sdevelopmentmaven;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Repository
public class FileManager {

    @Value("${file.path}")
    private String path;

    public List<String> getContent() {
        var filePath = Paths.get(path + "/file.txt");

        try {
            return Files.readAllLines(filePath);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new ArrayList<>();
    }

}
