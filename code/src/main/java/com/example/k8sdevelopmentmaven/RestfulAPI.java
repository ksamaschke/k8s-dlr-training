package com.example.k8sdevelopmentmaven;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class RestfulAPI {

    @Value("${default.greeting:Hello World!}")
    private String greeting;

    private final FileManager fileManager;

    public RestfulAPI(FileManager fileManager) {
        this.fileManager = fileManager;
    }

    @GetMapping("/")
    public String getGreeting() {
        return greeting;
    }

    @GetMapping("/file")
    public ResponseEntity<List<String>> getFileContents() {
        var fileContents = fileManager.getContent();

        return ResponseEntity.ok(fileContents);
    }

}