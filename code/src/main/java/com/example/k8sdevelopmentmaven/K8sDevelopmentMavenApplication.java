package com.example.k8sdevelopmentmaven;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class K8sDevelopmentMavenApplication {

	public static void main(String[] args) {
		var app = new SpringApplication(K8sDevelopmentMavenApplication.class);

		// Add ShutdownHook
		app.addListeners(new ShutdownHook());

		app.run(args);
	}

}
