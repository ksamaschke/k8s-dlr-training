# Unterlagen und Codes Kubernetes

## Verzeichnis _k8s_
Kubernetes Deployment-Beispiele

## Verzeichnis _code_
Java-Beispiel mit Spring Boot. Beinhaltet ein HELM chart, um die Anwendung in Kubernetes zu deployen. Dateizugriff auf ein gemountetes Volume. Dockerfile für die Erstellung des Images.

### Standard Volume-Mount 
Wird lokal unter `/tmp/data` erwartet.

## Verzeichnis _presentation_
Präsentationen zu Kubernetes

## Voraussetzungen
- Docker oder Podman
- Kubernetes (z.B. MicroK8s, Minikube, Docker Desktop, ...)
- Helm
- Registry (hier wird die MicroK8S-Registry verwendet)
- JDK 17, um Beispiele zu bauen (kann aber auch im Container passieren)

## Kontakt
Karsten Samaschke, karsten.samaschke@cloudical.io

Mastodon: @karsten@samaschke.social